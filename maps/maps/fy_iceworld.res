// fy_iceworld.res - created with RESGen v2.0.2.
// RESGen is made by Jeroen "ShadowLord" Bogers,
// with serveral improvements and additions by Zero3Cool.
// For more info go to http://resgen.hltools.com

// .res entries (14):
fy_iceworld.wad
gfx/env/snowbk.tga
gfx/env/snowdn.tga
gfx/env/snowft.tga
gfx/env/snowlf.tga
gfx/env/snowrt.tga
gfx/env/snowup.tga
grid_cso.wad
models/bzm_metro/bg_plightB.mdl
models/bzm_metro/bg_plightR.mdl
overviews/fy_iceworld.bmp
overviews/fy_iceworld.txt
ze_hitchhiking.wad
zs_encounter.wad
